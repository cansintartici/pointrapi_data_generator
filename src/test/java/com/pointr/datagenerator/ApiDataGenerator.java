package com.pointr.datagenerator;

import com.pointr.dataprovider.APIDataProvider;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;

import com.pointr.utils.BaseTest;
import com.pointr.utils.GlobalConstant;
import com.pointr.utils.JsonFileUtil;
import com.pointr.utils.RestUtil;
import com.pointr.utils.Utilities;

import static org.hamcrest.Matchers.hasItem;


public class ApiDataGenerator extends BaseTest {

    /**
     * Sending POST request to load maps with valid data
     *
     * @throws Exception
     */
    @Test
    public void DATA_GENERATOR_Add_New_Map() throws Exception {
        Headers headers = getAllHeaders(authenticationToken, deviceIdentifier);
        // Send post request
        Response responseGetMap = given().headers(headers).when()
                .post(GlobalConstant.MapsLoadMaps + "?"+GlobalConstant.Facility+"="+GlobalConstant.facilityIdentifier);
        String responseBody = responseGetMap.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);

        String listAsString = jsonPath.getString("body.data.level");

        listAsString = listAsString.replace("[", "");
        listAsString = listAsString.replace("]", "");
        listAsString = listAsString.replace(" ", "");
        //String[] parts = listAsString.split(",");
        int[] numbers = Arrays.stream(listAsString.split(",")).mapToInt(Integer::parseInt).toArray();
        int latestMap = numbers[numbers.length-1];
        GlobalConstant.mapLevelToAdd = latestMap+1;

        Response responseSetMap = given().headers(headers).when()
                .formParam("url", GlobalConstant.urlMap)
                .formParam("thumbUrl", GlobalConstant.thumbUrlMap)
                .formParam("description", GlobalConstant.description)
                .formParam("title", GlobalConstant.title)
                .formParam("level",GlobalConstant.mapLevelToAdd)
                .formParam("facilityIdentifier",GlobalConstant.facilityIdentifier)
                .post(GlobalConstant.SaveMaps);
        RestUtil.verifyPoiResponseData(responseSetMap, GlobalConstant.HTTP_OK, "");

    }
    /**
     *
     */
    @Test(dataProvider = "PoiAddData", dataProviderClass = APIDataProvider.class)
    public void DATA_GENERATOR_Add_Pois(String type, String json, boolean isArray) throws Exception {
        Headers headers = getAllHeaders(authenticationToken, deviceIdentifier);

        // Updating the description field in JSON
        String poi = "poi" + Utilities.getTimeStamp();
        JSONParser parser = new JSONParser();
        Object jsonObject = JsonFileUtil.updateJsonValue(parser.parse(json), "id", poi);

        String jsonString = jsonTOString(jsonObject);


        // Send post request
        Response response = given().headers(headers)
                .formParam("jsondata", jsonString)
                .formParam("isarray", isArray)
                .formParam("facilityIdentifier",GlobalConstant.facilityIdentifier)
                .when().post(GlobalConstant.MapDesignerSuffix);
        System.out.println(response.getBody().asString());


        // Assertion response
        RestUtil.verifyPoiResponseData(response, GlobalConstant.HTTP_OK, "");
    }

    @Test
    public void DATA_GENERATOR_Map_Designer_Publish() throws Exception {
        Headers headers = getAllHeaders(authenticationToken, deviceIdentifier);
        // Send post request
        Response responseMapDesigner = given().headers(headers).when()
                .post(GlobalConstant.MapDesignerPublish + "?"+GlobalConstant.Facility+"="+GlobalConstant.facilityIdentifier);

        RestUtil.verifyPoiResponseData(responseMapDesigner, GlobalConstant.HTTP_OK, "");

    }

    @Test
    public void DATA_GENERATOR_Maps_Publish() throws Exception {
        Headers headers = getAllHeaders(authenticationToken, deviceIdentifier);
        // Send post request
        Response responseMaps = given().headers(headers).when()
                .post(GlobalConstant.PublishMaps + "?"+GlobalConstant.Facility+"="+GlobalConstant.facilityIdentifier);

        RestUtil.verifyPoiResponseData(responseMaps, GlobalConstant.HTTP_OK, "");

    }



}
