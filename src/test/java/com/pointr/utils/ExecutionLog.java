package com.pointr.utils;

import java.io.BufferedWriter;
//import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
//import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.jsoup.Connection.Request;


import io.restassured.RestAssured;
import io.restassured.response.Response;

public class ExecutionLog {
	public static void Log(String text) {
		try {
			ExecutionLog executionLog = new ExecutionLog();
			String dateTime = executionLog.getDate();
			String fileName = executionLog.getFileName();
			// Create file
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + text;
			out.write(text);
			out.newLine();

			// Close the output stream
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void LogExceptionMessage(Exception e) {

		try {
			ExecutionLog executionLog = new ExecutionLog();
			String dateTime = executionLog.getDate();
			ExecutionLog.Log(dateTime
					+ " [info]  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Error message >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			String fileName = executionLog.getFileName();
			PrintWriter pw;
			pw = new PrintWriter(new FileWriter("ExecutionLog//" + fileName + ".txt", true));
			e.printStackTrace(pw);
			pw.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void LogErrorMessage(Error e) {
		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		ExecutionLog.Log(
				dateTime + " [info]  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Error message >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		String fileName = executionLog.getFileName();
		PrintWriter pw;
		try {
			pw = new PrintWriter(new FileWriter("ExecutionLog//" + fileName + ".txt", true));
			e.printStackTrace(pw);
			pw.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public String getFileName() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		String fileName = "Report-" + dateFormat.format(cal.getTime());
		return fileName;
	}

	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String dateTime = dateFormat.format(cal.getTime());
		return dateTime;
	}

	public static void LogResponse(Response response) {
		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		Request request = null;

		try {
			// Create file
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("Response Body=" + response.body().print());
			out.newLine();
			out.write("Response Code=" + response.statusCode());
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			// Close the output stream
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void LogEndClass(String text) {
		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			// Create file
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  End Execution of Test Class " + text;
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			// Close the output stream
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void LogMapDesignerRequest(String text, String jsonBody, Boolean isArray, String requestType,
			String authenticationToken, String deviceIdentifier, String uriSubPart) {

		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {

			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("------Body Form Parameters------");
			out.newLine();
			out.write("jsondata:" + jsonBody);
			out.newLine();
			out.write("isarray:" + isArray);
			out.newLine();
			out.write("facilityIdentifier: 1");
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			// Close the output stream
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	public static void logGetRequestAndResponse(String text, Response response, String uriSubPart, String requestType,
			String authenticationToken, String deviceIdentifier) throws IOException {
		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("---------------------------------------");
			out.newLine();
			out.write("Response Body=" + response.body().print());
			out.newLine();
			out.write("Response Code=" + response.statusCode());
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			// Close the output stream
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void logConfigurationPostRequest(String text, String parameterDescription, String parameterValue,
			String parameterDataType, String requestType, String uriSubPart, String authenticationToken,
			String deviceIdentifier, int... id) {
		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("------Body Form Parameters------");
			out.newLine();
			out.write("ParameterDescription: " + parameterDescription);
			out.newLine();
			out.write("ParameterValue: " + parameterValue);
			out.newLine();
			out.write("ParameterDataType: " + parameterDataType);
			out.newLine();
			if (uriSubPart == GlobalConstant.EditConfiguration) {
				out.write("id:" + id[0]);
				out.newLine();
			}
			if (uriSubPart == GlobalConstant.DeleteConfiguration) {
				out.write("------Pass following parameters with URI----------");
				out.newLine();
				out.write("?configurationId=" + id[0]);
				out.newLine();
				out.write("&facilityIdentifier=1");
				out.newLine();
			}
			if (uriSubPart == GlobalConstant.EditGlobalConfiguration) {
				out.write("id:" + id[0]);
				out.newLine();
			}
			out.write("*****************************************************************************");
			out.newLine();
			// Close the output stream
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void logConfigurationDeleteRequest(String text, String requestType, String uriSubPart,
			String authenticationToken, String deviceIdentifier, String id) {
		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("------Pass following parameters with URI----------");
			out.newLine();
			out.write("?configurationId=" + id);
			out.newLine();
			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	public static void logAccountPostRequest(String text, String requestType, String uriSubPart, String userId,
			String roleId, String recoveryKey, String newPassword, String oldPassword, String confirmPassword,
			String authenticationToken, String deviceIdentifier,String... optionalParam) {

		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();

			if (uriSubPart == GlobalConstant.AccountAssignRole) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("UserId=" + userId);
				out.newLine();
				out.write("RoleId=" + roleId);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AccountChangePassword) {

				out.write("------Form parameters----------");
				out.newLine();
				out.write("UserId=" + userId);
				out.newLine();
				out.write("recoveryKey=" + recoveryKey);
				out.newLine();
				out.write("newPassword=" + newPassword);
				out.newLine();
				out.write("oldPassword=" + oldPassword);
				out.newLine();
				out.write("confirmPassword=" + confirmPassword);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AccountRetrieveUserProfileOptions) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("UserId=" + userId);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AccountRegisterDevice) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("deviceIdentifier=" + userId);
				out.newLine();
				out.write("deviceModel=" + roleId);
				out.newLine();
				out.write("manufacturer=" + recoveryKey);
				out.newLine();
				out.write("os=" + newPassword);
				out.newLine();
				out.write("version=" + oldPassword);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.EntryExitLoadVisits) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("startindex=" + userId);
				out.newLine();
				out.write("count=" + roleId);
				out.newLine();

			}

			else if (uriSubPart == GlobalConstant.RetrieveHeatMapData) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("level=" + userId);
				out.newLine();
				out.write("heatMapType=" + roleId);
				out.newLine();
				out.write("positionType=" + recoveryKey);
				out.newLine();
				out.write("timeOffset=" + newPassword);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.SaveMaps) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("url=" + userId);
				out.newLine();
				out.write("thumbUrl=" + roleId);
				out.newLine();
				out.write("description=" + recoveryKey);
				out.newLine();
				out.write("title=" + newPassword);
				out.newLine();
				out.write("level=" + oldPassword);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.EditMap) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("description=" + userId);
				out.newLine();
				out.write("title=" + roleId);
				out.newLine();
				out.write("level=" + recoveryKey);
				out.newLine();

			}

			else if (uriSubPart == GlobalConstant.UpdateFilterParameters) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("level=" + userId);
				out.newLine();
				out.write("positionType=" + roleId);
				out.newLine();
				out.write("durationValue=" + recoveryKey);
				out.newLine();
				out.write("durationType=" + newPassword);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AddFacility) {

				out.write("------Form parameters----------");
				out.newLine();
				out.write("Description=" + userId);
				out.newLine();
				out.write("ExternalIdentifier=" + recoveryKey);
				out.newLine();
				out.write("InternalIdentifier=" + newPassword);
				out.newLine();
				out.write("VenueId=" + oldPassword);
				out.newLine();
				out.write("utcTimeOffset=" + confirmPassword);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.EditCountry) {

				out.write("------Form parameters----------");
				out.newLine();
				out.write("id=" + userId);
				out.newLine();
				out.write("name=" + recoveryKey);
				out.newLine();
				out.write("isEnabled=" + newPassword);
				out.newLine();

			}
			
			else if (uriSubPart == GlobalConstant.EditFacilities) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("id=" + userId);
				out.newLine();
				out.write("description=" + roleId);
				out.newLine();
				out.write("ExternalIdentifier=" + recoveryKey);
				out.newLine();
				out.write("InternalIdentifier=" + newPassword);
				out.newLine();
				out.write("VenueId=" + oldPassword);
				out.newLine();
				out.write("utcTimeOffset=" + confirmPassword);
				out.newLine();
				out.write("isDefault=" + optionalParam);
				out.newLine();
			}
			
			else if (uriSubPart == GlobalConstant.RegisterUser) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("userName=" + userId);
				out.newLine();
				out.write("password=" + roleId);
				out.newLine();
				out.write("confirmPassword=" + recoveryKey);
				out.newLine();
				out.write("role_Id=" + newPassword);
				out.newLine();
				
			}

			else if (uriSubPart == GlobalConstant.UpdateRegisteredUser) {
				out.write("------Form parameters----------");
				out.newLine();
				out.write("userName=" + userId);
				out.newLine();
				out.write("password=" + roleId);
				out.newLine();
				out.write("confirmPassword=" + recoveryKey);
				out.newLine();
				out.write("role_Id=" + newPassword);
				out.newLine();
				
			}

			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	public static void LogAddNewRolePostRequest(String text, String requestType, String uriSubPart, String newRole,
			String authenticationToken, String deviceIdentifier) {

		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("------Form parameters----------");
			out.newLine();
			out.write("newRole=" + newRole);
			out.newLine();

			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	public static void LogPostRequestAndResponse(String text, String requestType, String uriSubPart, String paramName,
			String authenticationToken, String deviceIdentifier) {

		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("------Form parameters----------");
			out.newLine();

			if (uriSubPart == GlobalConstant.AccountDevicePushActivity) {
				out.write("emailAddress=" + paramName);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.RecoverAccountPassword) {
				out.write("emailAddress=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.ValidateAccountApiKey) {
				out.write("key=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.PublishConfiguration
					|| uriSubPart == GlobalConstant.RetrieveServiceAllFloors) {
				out.write("facilityIdentifier=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.RetrieveConfigurationFacilities) {
				out.write("facility=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();

			}

			else if (uriSubPart == GlobalConstant.CustomerFeedbacks) {

				out.write("feedbackMessage=" + paramName);
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AddCountry) {
				out.write("name=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();

			}

			else if (uriSubPart == GlobalConstant.SavePrefixes) {
				out.write("value=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();

			}

//			else if (uriSubPart == GlobalConstant.UnregisteredDevicePushToken) {
//				out.write("pushToken=" + paramName);
//				out.newLine();
//				out.write("----------------------------");
//				out.newLine();
//
//			}

			else if (uriSubPart == GlobalConstant.RegisterDevicePushToken) {
				out.write("pushToken=" + paramName);
				out.newLine();
				out.write("----------------------------");
				out.newLine();

			}

			else if (uriSubPart == GlobalConstant.PublishGraph) {
				out.write("facilityIdentifier" + paramName);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.DeleteMap) {
				out.write("level" + paramName);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}
			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	public static void LogPostRequestAndResponse(String text, String requestType, String uriSubPart, String firstParam,
			String secondParam, String authenticationToken, String deviceIdentifier) {

		ExecutionLog executionLog = new ExecutionLog();
		String dateTime = executionLog.getDate();
		String fileName = executionLog.getFileName();
		try {
			FileWriter fstream = new FileWriter("ExecutionLog//" + fileName + ".txt", true);
			BufferedWriter out = new BufferedWriter(fstream);
			text = dateTime + " [info]  " + " Execution Started of Test Class " + text;
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write(text);
			out.newLine();
			out.write("*****************************************************************************");
			out.newLine();
			out.write("------------Request and Response Content-----------");
			out.newLine();
			out.write("Method:" + requestType);
			out.newLine();
			out.write("URI=" + RestAssured.baseURI.toString() + uriSubPart);
			out.newLine();
			out.write("----------------Headers----------");
			out.newLine();
			out.write("AuthenticationToken:" + authenticationToken);
			out.newLine();
			out.write("DeviceIdentifier:" + deviceIdentifier);
			out.newLine();
			out.write("Content-Type:application/x-www-form-urlencoded");
			out.newLine();
			out.write("------Form parameters----------");
			out.newLine();
			if (uriSubPart == GlobalConstant.LoadSimulationData) {
				out.write("startDate:" + firstParam);
				out.newLine();
				out.write("endDate" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.RetrieveConfigurationBlackList) {
				out.write("id:" + firstParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.RemoveUserFacilities) {
				out.write("userId:" + firstParam);
				out.newLine();
				out.write("facilityIdentifier" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AddUserFacilities) {
				out.write("userId:" + firstParam);
				out.newLine();
				out.write("facilityIdentifier" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.RemoveRoleActivity) {
				out.write("activity[]:" + firstParam);
				out.newLine();
				out.write("roleId" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.AddRoleActivities) {
				out.write("activity[]:" + firstParam);
				out.newLine();
				out.write("roleId" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.ConfigurationEditPrefixes) {
				out.write("Id:" + firstParam);
				out.newLine();
				out.write("value:" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			else if (uriSubPart == GlobalConstant.LoadMessageJobs) {
				out.write("StartIndex:" + firstParam);
				out.newLine();
				out.write("count:" + secondParam);
				out.newLine();
				out.write("--------------------------------");
				out.newLine();
			}

			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	public static void main(String[] str) {
		Log("Test execution");
	}

}
