package com.pointr.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class GlobalConstant {

	public final static String resource = "src/test/resources/";
	public final static String inputFile = resource + "inputfiles/";
	public final static String outputFile = resource + "/outputfile/";
	public final static String poiInputs = resource + "/poiInputData/";
	public final static String poiinput = inputFile + "/poi/";
	public final static String building = inputFile + "/building/";
	public final static String mapDesigner = inputFile + "/mapDesigner/";
	public final static String facilityIdentifier = "1";
	public final static String InternalIdentifierId = "1";
	public final static String InternalIdentifier="facilityInternalIdentifier=";
	public final static String Facility="facilityIdentifier";
	public final static String VenueIdentifier="venueInternalIdentifier=";
	public final static String VenuIdentifierId="1";
	public final static String User="userId";
	public final static String UserId="22";
	public final static String countryID="2";
	public final static String roleId="1";
	public final static String recoveryKey = "fromDashboard";
	public final static String accountDeviceIdentifier= "64EE7CA5-0913-46F2-A326-506752BFEB17";
	public final static String DeviceIdentifier= "64EE7CA5-0913-46F2-A326-506752BFEB17";
	public static String accountAuthenticationToken= null;
	public static List<String> globalConfigurationIds= new ArrayList<String>();
	public static List<String> configurationIds= new ArrayList<String>();
	public static Integer counterWebConfig= 0;
	public static List<Integer> webConfigs = new ArrayList<Integer>();
	public static String accountPushToken= "64EE7CA5";
	public String prefixName= "TestPrefix";

	//Data Generator
	public static String urlMap = "https://pointrimages.blob.core.windows.net/dxb/1524500347645830.jpg";
	public static String thumbUrlMap = "https://pointrimages.blob.core.windows.net/dxb/low_1524500347645830.jpg";
	public static String description = "Added by API test";
	public static String title = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
	public static int mapLevelToAdd;
	public static String poiId = "poi"+new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
	public static String portalId = "group"+new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());



	// Url of POI test method
	public static final String BUILDERSuffix = "/building";
	private static final String facilities = "?facilityIdentifier=1";
	public static final String PoISaveSuffix = BUILDERSuffix + "/savedata" + facilities;
	public static final String PoIEditSuffix = BUILDERSuffix + "/editdata" + facilities;
	public static final String PoIpublishSuffix = "/poi/publish" + facilities;
	public static final String PoIDeleteSuffix = BUILDERSuffix + "/deletedata" + facilities;
	public static final String SavePOI = "/SavePoi";
	public static final String EditPoi = "/EditPoi";
	public static final String DeletePoi = "/DeletePoi";
	public static final String PoiTypes="/PoiTypes";

	public static final String ExportData = "/exportdata/retrievepoidata";

	// Url of Builder test method
	public static final String BuildingSuffix = BUILDERSuffix + "/savedata" + facilities;
	public static final String EditBuildingSuffix = BUILDERSuffix + "/editdata" + facilities;
	public static final String DeleteBuilderSuffix = BUILDERSuffix + "/deletedata" + facilities;
	public static final String PublishBuilderSuffix = BUILDERSuffix + "/publish" + facilities;

	// Url of Map designer
	public static final String MapDesigner = "/MapDesigner";
	public static final String MapDesignerPublish = "/MapDesigner/Publish";
	public static final String MapDesignerSuffix = MapDesigner + SavePOI;
	public static final String MapDesignerPublishSuffix = MapDesigner + "/publish" + facilities;
	public static final String MapDesignerEditSuffix = MapDesigner + EditPoi;
	public static final String DeleteMapDesigner = MapDesigner + DeletePoi;
	public static final String RetrieveConfigurationDataForExport="/ExportData/RetrieveConfigurationData";
	public static final String RetrieveExportGraphData="/ExportData/RetrieveGraphData";
	public static final String RetrieveExportBeaconData="/ExportData/RetrieveIBeaconData";
	public static final String RetrieveExportPopDeviceData="/ExportData/RetrievePopDeviceData";
	public static final String RetrieveExportPopHealthStatus="/ExportData/RetrievePopHealthStatus";
	public static final String RetrieveExportPositionDataYear="/ExportData/RetrievePositionDataYear";
	public static final String RetrieveExportWallData="/ExportData/RetrieveWallData";
	public static final String RetrieveExportZoneData="/ExportData/RetrieveZoneData";
	public static final String RetrieveMapDesignerPoiTypes=MapDesigner+PoiTypes;
	public static final String RetrieveExportPositionDataForInterval="/ExportData/RetrievePositionDataForSelectedInterval";
	public static final String RetrieveExportReceivedData="/ExportData/RetriveReceivedData";
	public static final String RetrieveExportFootfallData="/ExportData/RetriveFootfallData";
	public static final String RetrieveExportPositionData="/ExportData/RetrievePositionData";
	public static final String RetrieveExportDataSdkCrashLogs="/ExportData/RetrieveSdkCrashLogs";
	public static final String RetrieveExportVersionData="/ExportData/RetrieveVersionData";
	public static final String RetrieveExportData="/ExportData/RetrieveExportData";
	public static final String RetrieveExportNotificationData="/ExportData/RetrieveNotificationData";
	public static final String RetrieveMapDesignerPoi=MapDesigner+"/poi";
	public static final String RetrieveExportSelfServeData="/ExportData/RetrieveSelfServeData";
	

	// url for configure
	public static final String RetrieveWebConfiguration = "/Configuration/LoadWebConfigData";
	public static final String GetTabs = "Configuration/GetTabs";
	public static final String RetrievePopStatus = "/Configuration/RetrievePopStatus";
	public static final String RetrieveWebJobStatus = "/Configuration/RetrieveWebJobStatus";
	public static final String RetrieveExceptionLogs = "/Configuration/RetrieveExceptionLogs";
	public static final String RetrieveSystemActivities = "/Configuration/RetrieveSystemActivities";
	public static final String RetrieveGlobalConfiguration = "/Configuration/LoadGlobalConfigurationData";
	public static final String RetrieveBeaconStatus = "/Configuration/RetrieveBeaconStatus";
	public static final String RetrievePopConfiguration = "/Configuration/RetrieveSnifferConfigurationData";
	public static final String DeleteDevice = "/Configuration/DeleteDevice";
	public static final String RetrieveConfiguration = "/Configuration/LoadConfigurationData";
	public static final String RetrieveUsers = "/Configuration/RetrieveUsers";
	public static final String RetrieveSDKCrashLogs = "/Configuration/RetrieveSdkCrashLogs";
	public static final String SaveWebConfiguration = "/Configuration/SaveWebConfigData";
	public static final String EditWebConfiguration = "/Configuration/EditGlobalConfigurationData";
	public static final String DeleteWebConfiguration = "/Configuration/DeleteWebConfigData";
	public static final String SaveGlobalConfiguration = "/Configuration/SaveGlobalConfigurationData";
	public static final String SaveConfiguration = "/Configuration/SaveConfigurationData";
	public static final String DeleteGlobalConfiguration = "/Configuration/DeleteConfigurationData";
	public static final String EditGlobalConfiguration = "/Configuration/EditGlobalConfigurationData";
	public static final String EditConfiguration = "/Configuration/EditConfigurationData";
	public static final String DeleteConfiguration = "/Configuration/DeleteConfigurationData";
	public static final String RegisterUser = "/Account/RegisterUser";
	public static final String UpdateRegisteredUser = "/Account/UpdateUser";
	public static final String RetrieveUserNames="/Configuration/GetUserNamesStartWith";
	public static final String RetrieveMasterPopIpAddress="/Configuration/RetrieveMasterPopIpAddress";
	public static final String RetrievePoiTypeProperties="/Configuration/RetrievePoiTypeProperties";
	public static final String RetrievePoiTypes="/Configuration/RetrievePoiTypes";
	public static final String RetrievePoiTypePropertyTuples="/Configuration/RetrievePoiTypePropertyTuples";
	public static final String RetrieveSelectedPoiTypePropertyTuple="/Configuration/RetrieveSelectedPoiTypePropertyTuple";
	public static final String RetrieveConfigurationActivities="/Configuration/RetrieveActivities";
	public static final String RetrieveConfigurationOtherActivities="/Configuration/RetrieveOtherActivities";
	public static final String RetrieveConfigurationExportSdkCrashLogs="/Configuration/ExportSdkCrashLogs";
	public static final String RetrieveConfigurationFacilities="/Configuration/RetrieveFacilities";
	public static final String RetrieveConfigurationServerTimeStamp="Configuration/RetrieveServerTimestamp";
	public static final String RetrieveConfigurationUsers="/Configuration/RetrieveUsers";
	public static final String ConfigurationAddRole="/Configuration/AddNewRole";
	public static final String PublishConfiguration="/Configuration/Publish";
	public static final String RetrieveConfigurationPrefixes="/Configuration/RetrievePrefixes";
	public static final String ConfigurationEditPrefixes="/Configuration/EditPrefix";
	public static final String LoadUserActivityData="//UserActivity/LoadData";
	public static final String RetrieveConfigurationBlackList="/Configuration/RetrieveBlackList";
	public static final String AddRoleActivities="/Configuration/AddRoleActivities";
	public static final String RemoveRoleActivity="/Configuration/RemoveRoleActivities";
	public static final String AddUserFacilities="/Configuration/AddUserFacility";
	public static final String RemoveUserFacilities="/Configuration/RemoveUserFacility";
	public static final String AddFacility="/Configuration/AddFacility";
	public static final String RetrieveCountriesList="/Configuration/RetrieveCountries";
	public static final String RetrieveUserFacilities="/Configuration/RetrieveUserFacilities";
	public static final String RetrieveUserFacilitiesSelect="/Configuration/RetrieveUserFacilitiesSelect";
	public static final String RetrieveConfigurationVenues="/Configuration/RetrieveVenues";
	public static final String AddCountry="/Configuration/AddCountry";
	public static final String EditCountry="/Configuration/EditCountry";
	public static final String SavePrefixes="/Configuration/SavePrefix";
	public static final String DeletePrefix="/Configuration/DeletePrefix";
	public static final String EditPrefix="/Configuration/EditPrefix";
	public static final String EditFacilities="/Client/EditFacility";
	public static final String RetrieveServerTimestamp="/Configuration/RetrieveServerTimestamp";

	
	// url for Analytics
	public static final String RetrieveAnalyticsDeviceVendors = "/Analytics/DeviceVendors";
	public static final String RetrieveAnalyticsRealTime = "/Analytics/RealTime";
	public static final String RetriveAnalyticsSubFacilities = "/Analytics/SubFacilities";
	public static final String RetrieveAnalyticsChartsMeta="/Analytics/ChartsMeta";
	public static final String RetrieveAnalyticsDailyVisitDistribution="/Analytics/DailyVisitDistribution";
	public static final String RetrieveAnalyticsHourlyVisitDistribution="/Analytics/HourlyVisitDistribution";
	public static final String RetrieveAnalyticsMonthlyVisits="/Analytics/MonthlyVisits";
	public static final String RetrieveAnalyticsVisitConversion="/Analytics/VisitConversion";
	public static final String RetrieveAnalyticsVisitDuration="/Analytics/VisitDuration";
	public static final String RetrieveAnalyticsWeeklyVisits="/Analytics/WeeklyVisits";	
	public static final String RetrieveAnalyticsSegmentsByFacility="/Analytics/SegmentsByFacility";
	public static final String RetrieveAnalyticsVisitOverview="/Analytics/VisitOverview";

	//url for Asset
	public static final String RetrieveAssets = "/Asset/GetAssets";
	public static final String RetrieveAssetUsers= "/Asset/GetAssetUsers";


	// url for Account
	public static final String RetrieveAccountFillRoles = "/Account/FillRoles";
	public static final String RetrieveAccountUserExtraData="/Account/GetUserExtraData";
	public static final String RetrieveAccountMD5="/Account/MD5";
	public static final String AccountAssignRole="/Account/AssignRole";
	public static final String AccountUnassignRole="/Account/UnassignRole";
	public static final String AccountChangePassword="/Account/ChangePassword";
	public static final String AccountEnableDevice="/Account/EnableDevice";
	public static final String AccountRetrieveUserProfileOptions="/Account/RetrieveUserProfileOptions";
	public static final String AccountDevicePushActivity="/Account/UpdateDevicePushActivity";
	public static final String RecoverAccountPassword="/Account/RecoverPassword";
	public static final String ValidateAccountApiKey="/Account/ValidateApiKey";
	public static final String AccountRegisterDevice="/Account/RegisterDevice";
	public static final String RegisterDevicePushToken="/Account/RegisterDevicePushToken";
	public static final String UnregistereDevicePushToken="/Account/UnregisterDevicePushToken";
	
	//url of HeatMap
	public static final String RetrieveHeatMapFacilityOffset="/HeatMap/RetrieveSelectedFacilityOffset";
	public static final String RetrieveHeatMapData="/HeatMap/RetrieveHeatMapData";
	
	//url for Service
	public static final String RetrieveServicePositionZonePois="/Service/LoadPositionZonePois";
	public static final String RetrieveServiceOption="/Service/RetrieveOptions";
	public static final String RetrieveServiceProfileOptions="/Service/RetrieveProfileOptions";
	public static final String RetrieveServiceAllFloors="/Service/RetrieveAllFloors";
	public static final String LoadServiceFacility="/Service/LoadFacility";
	
	//url for tracking
	public static final String RetrieveTrackingBeaconData="/Tracking/ExtractBeaconDataFromSniffedData";
	public static final String RetrieveTrackingSniffedDataInInterval="/Tracking/RetrieveSniffedDataInInterval";
	
	//url for graph
	public static final String RetrieveGraphPoiTypesItemList="/Graph/RetrieveGraphPoiTypesItemList";
	public static final String PublishGraph="/Graph/Publish";
	public static final String RetrieveGraphNodes="/Graph/LoadGraphNodes";
	
	//url for SdkAnalytics
	public static final String RetrieveSdkAnalyticsCountOfPath="/SdkAnalytics/GetCountOfPathCalculations";
	public static final String RetrieveSdkAnalyticsCountOfPoiSearches="/SdkAnalytics/GetCountOfPoiSearches";
	public static final String RetrieveSdkAnalyticsNumberOfUsesOfSdk="/SdkAnalytics/GetNumberOfUsesOfSdk";
	public static final String RetrieveSdkAnalyticsUniqueUserNumber="/SdkAnalytics/GetUniqueUserNumber";
	public static final String RetrieveSdkAnalyticsYearsForCharts="/SdkAnalytics/GetYearsForCharts";
	public static final String RetrieveSdkAnalyticsWeeksForCharts="/SdkAnalytics/GetWeeksForCharts";
	
	//url for Notifications
	public static final String RetrieveNotificationUserNamesStartWith="/Notifications/GetUserNamesStartWith";
	public static final String NotificationsLoadAssetUsers="/Notifications/LoadAssetUsers";
	public static final String NotificationsLoadPois="/Notifications/LoadPois";
	public static final String RetrieveNotificationsSentMessageDatas="Notifications/RetrieveSentMessageDatas";
	
	//url for Campaign Management
	public static final String RetrieveCampaignAvarage="/CampaignManagement/GetCampaignAvarage";
	public static final String RetrieveCampaignDetails="/CampaignManagement/GetCampaignDetails";
	public static final String RetrieveCampaignDetailsWeeklyBreakdown="/CampaignManagement/GetCampaignDetailsWeeklyBreakdown";
	public static final String RetrieveExistingCampaign="/CampaignManagement/GetExistingCampaigns";
	public static final String RetrieveOverallCampaignInformation="/CampaignManagement/GetOverallCampaignInfo";
	
	//url for Listeners
	public static final String RetrieveListenerDevicePositions="/Listener/RetrieveDevicePositions";
	
	//url for Guru
	public static final String RetrieveGuruListBeacons="/Guru/ListBeacons";
	
	//url for customer
	public static final String CustomerFeedbacks="/CustomerFeedbacks/Create";
	public static final String RetrieveCustomerFeedbacks="/CustomerFeedbacks/Load";	
	
	//url for entry exit
	public static final String EntryExitLoadDuration="/EntryExit/LoadDurationAnalytics";
	public static final String EntryExitLoadFrequency="/EntryExit/LoadFrequencyAnalytics";
	public static final String EntryExitVisitAnalytics="EntryExit/LoadVisitAnalytics";
	public static final String EntryExitLoadVisits="/EntryExit/LoadVisits";
	
	//url for Maps
	public static final String MapsLoadMaps="/Maps/LoadMaps";
	public static final String PublishMaps="/Maps/Publish";
	public static final String MapsCancelProcedure="/Maps/CancelProcedure";
	public static final String SaveMaps="/Maps/SaveMap";
	public static final String EditMap="/Maps/EditMap";
	public static final String DeleteMap="/Maps/DeleteMap";
	
	//url for positions
	public static final String RetrievePositionsData="/Positions/RetrieveData";
	public static final String RetrievePositionsFilteredLastPosition="/Positions/RetrieveFilteredLastPosition";
	public static final String UpdateFilterParameters="/Positions/UpdateFilterParameters";
	
	//url for simulations
	public static final String LoadSimulationData="/Simulation/LoadData";
	
	//url for Message
	public static final String LoadMessageJobs="/MessageJob/LoadMessageJobs";
	
	// Http response Code
	public static final int HTTP_OK = 200;
	public static final int HTTP_BAD_REQUEST = 400;
	public static final int HTTP_UNAUTHORIZED = 401;
	public static final int HTTP_INTERNAL_SERVER_ERROR = 500;
	public static final int HTTP_NOT_FOUND = 404;
	public static final int HTTP_NOT_ACCEPTABLE = 406;
	public static final int HTTP_FORBIDDEN = 403;
	public static final int HTTP_SERVICE_UNAVAILABLE = 503;

	// Global Response Messages
	public static final String JSON_NOT_PARSE = "Json string cannot be parsed";
	public static final String AUTHENTICATION_REQUIRED = "Authentication required";
	public static final String POI_EXIST = "poi already exists";
	public static final String INVALID_POI = "Invalid poi code";
	public static final String INVALID_JSON = "Invalid json string";
	public static final String POI_NOT_FOUND = "Poi could not be found";
	public static final String InternalServerError = "Internal Server Error";
	public static final String ENTITY_VALIDATION_ERROR = "Validation failed for one or more entities. See 'EntityValidationErrors' property for more details.";
	public static final String TRIGGER_VALIDATION = "shape is not supported for Trigger";
	public static final String SOLIDWALL_VALIDATION = "shape is not supported for Solid Wall";
	public static final String OBJECT_REFERENCE_NOT_SET = "Object reference not set to an instance of an object.";
	public static final String SuccessMsg = "Succeed";
	public static final String EXISTING_PARAMETER_NAME = "Configuration Data with the same description already exists, please use update button.";
	public static final String DATA_NOT_FOUND = "No such configuration data found!";
	public static final String DESCRIPTION_REQUIRED = "Description is required";
	public static final String VALUE_NOT_COMPATIBLE = "Value is not compatible with data type";
	public static final String VALUE_REQUIRED = "Value is required";
	public static final String USER_REGISTERED = "New user successfully registered";
	public static final String MAXIMUM_LENGTH = "The field userName must be a string or array type with a maximum length of '100'.";
	public static final String USER_NAME_REQUIRED = "User Name is required";
	public static final String PASSWORD_REQUIRED = "Password is required";
	public static final String PASSWORD_LENGTH = "Password must be at least 6 characters and at most 100 characters long.";
	public static final String PASSWORD_NOT_MATCHED = "The password and confirmation password do not match.";
	public static final String INFORMATION_UPDATED = "You are successfully registered";
	public static final String No_SUCH_CONFIGURATION = "No such Configuration Data founD!";
	public static final String EXSISTING_WEB_CONFIG = "Configuration data with same description alredy exists, please use update button.";
	public static final String FACILITY_NOT_FOUND = "Facility or Venue not found, facilityInternalIdentifier: venueInternalIdentifier:";

	public enum FileName {

		PoiJsonData("PoiJsonData"), PoiInputOne("PoiJsonSample_1"), PoiInputTwo("PoiJsonSample_2"), BuilingJsonFile(
				"BuildingData"), MapDesignerFile("MapDesignerData");

		private String value;

		private FileName(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public enum JsonProperties {

		Name("name"), Description("description"), Type("type"), Keywords("keywords"), Level("level"), ID("id"), Group(
				"group");

		private String value;

		private JsonProperties(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

}
